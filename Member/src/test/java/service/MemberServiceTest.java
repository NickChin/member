package service;

import domain.Member;
import domain.Student;
import domain.Teacher;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import repository.MemberRepository;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = MemberService.class)
class MemberServiceTest {

    @Autowired
    private MemberService memberService;

    @Mock
    private MemberRepository memberRepository;

    private List<Member> memberList;
    private Member member;
    private List<Teacher> teacherList;
    private Teacher teacher;
    private List<Student> studentList;
    private Student student;

    @BeforeEach
    void setup() throws Exception {
        memberList = new ArrayList<Member>();
        member = new Member();

        teacherList = new ArrayList<Teacher>();
        teacher = new Teacher();
        teacher.setId("1");
        teacher.setName("Billy");
        teacher.setGender("male");
        teacher.setSubject("數學");
        teacher.setJobTitle("教務主任");
        teacherList.add(teacher);

        studentList = new ArrayList<Student>();

        when(memberRepository.getAllMember()).thenReturn(memberList);
        when(memberRepository.getAllTeacher()).thenReturn(teacherList);
        when(memberRepository.getAllStudent()).thenReturn(studentList);
        when(memberRepository.getTeacher(Mockito.anyString())).thenReturn(teacherList);
        when(memberRepository.getStudent(Mockito.anyString())).thenReturn(studentList);
        doNothing().when(memberRepository).add(Mockito.anyString());
    }

    @Test
    void getAllMember() throws Exception{
        memberList = memberService.getAllMember();
    }

    @Test
    void getAllTeacher() throws Exception{
        teacherList = memberService.getAllTeacher();
    }

    @Test
    void getAllStudent() throws Exception {
        studentList = memberService.getAllStudent();
    }

    @Test
    void getTeacher() throws Exception {
        teacherList = memberService.getTeacher(Mockito.anyString());
    }

    @Test
    void getStudent() throws Exception {
        studentList = memberService.getStudent(Mockito.anyString());
    }

    @Test
    void add() throws Exception {
        memberService.add(Mockito.anyString());
    }
}