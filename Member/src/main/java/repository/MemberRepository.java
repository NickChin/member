package repository;

import domain.Member;
import domain.Student;
import domain.Teacher;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

import javax.persistence.Query;
import java.util.List;

public class MemberRepository implements PanacheRepositoryBase<Member, String> {

    public List<Member> getAllMember() {
        Query query = getEntityManager().createNativeQuery(
                "SELECT * FROM MEMBER ", Member.class);
        return query.getResultList();
    }

    public List<Teacher> getAllTeacher() {
        Query query = getEntityManager().createNativeQuery(
                "SELECT * FROM MEMBER WHERE JOB_TITLE IS NOT NULL", Teacher.class);
        return query.getResultList();
    }

    public List<Student> getAllStudent() {
        Query query = getEntityManager().createNativeQuery(
                "SELECT * FROM MEMBER WHERE JOB_TITLE IS NULL", Student.class);
        return query.getResultList();
    }

    public List<Teacher> getTeacher(String id) {
        Query query = getEntityManager().createNativeQuery(
                "SELECT * FROM MEMBER WHERE MEMBER_ID='"+id+"'", Teacher.class);
        return query.getResultList();
    }

    public List<Student> getStudent(String id) {
        Query query = getEntityManager().createNativeQuery(
                "SELECT * FROM MEMBER WHERE MEMBER_ID='"+id+"'", Student.class);
        return query.getResultList();
    }

    public Boolean add(String mb) {
        Boolean rs = false;
        Member member = new Member();
        Teacher teacher = new Teacher();
        Student student = new Student();
        String[] mbData;
        if (!mb.equals("")) {
            mbData = mb.split(",");
            member.setId(mbData[0]);
            member.setName(mbData[1]);
            member.setGender(mbData[2]);
            teacher.setSubject(mbData[3]);
            teacher.setJobTitle(mbData[4]);
            student.setAdmissionYearMonth(mbData[5]);
            student.setClas(mbData[6]);
            persist(member);
            rs = true;
        }
        return rs;
    }

}
