package domain;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.runtime.annotations.RegisterForReflection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MEMBER")
@RegisterForReflection
public class Member extends PanacheEntityBase implements Serializable {

    @Column(name="MEMBER_ID")
    private String id;
    @Column(name="MEMBER_NAME")
    private String name;
    @Column(name="GENDER")
    private String gender;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) { this.gender = gender;}

}
