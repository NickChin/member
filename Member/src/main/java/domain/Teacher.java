package domain;

import io.quarkus.runtime.annotations.RegisterForReflection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "MEMBER")
@RegisterForReflection
public class Teacher extends Member{

    @Column(name="SUBJECT")
    private String subject;
    @Column(name="JOB_TITLE")
    private String jobTitle;

    public String getSubject() {return subject;}

    public void setSubject(String subject) {this.subject = subject;}

    public String getJobTitle() {return jobTitle;}

    public void setJobTitle(String jobTitle) {this.jobTitle = jobTitle;}
}
