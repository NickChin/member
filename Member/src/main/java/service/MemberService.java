package service;

import domain.Member;
import domain.Student;
import domain.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import repository.MemberRepository;

import javax.ws.rs.POST;
import java.util.List;

public class MemberService {

    @Autowired
    MemberRepository MemberRepository;

    @POST
    public List<Member> getAllMember(){
        List<Member> result = MemberRepository.getAllMember();
        return result;
    }

    @POST
    public List<Teacher> getAllTeacher(){
        List<Teacher> result = MemberRepository.getAllTeacher();
        return result;
    }

    @POST
    public List<Student> getAllStudent(){
        List<Student> result = MemberRepository.getAllStudent();
        return result;
    }

    @POST
    public List<Teacher> getTeacher(String id){
        List<Teacher> result = MemberRepository.getTeacher(id);
        return result;
    }

    @POST
    public List<Student> getStudent(String id){
        List<Student> result = MemberRepository.getStudent(id);
        return result;
    }

    @POST
    public boolean add(String member){
        boolean rs = MemberRepository.add(member);
        return rs;
    }
}
