package Controller;

import domain.Member;
import domain.Student;
import domain.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import service.MemberService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/api/Member")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MemberController {

    @Autowired
    MemberService memberService;

    public void setInit(){
        String[] initData = //new String[4];
                {"1,Billy,male,數學,教務主任,,","2,Heidi,female,英文,教師,,","3,Jacky,male,,,201910,301","4,Lawrence,male,,,201812,801"};
        Member member = new Member();
        for(int i=0;i<initData.length;i++) {

            String mb = initData[i];
            String[] mbOne = mb.split(",");
//            for(int j=0;j< 1;j++) {
//                if (memberService.getStudent(mbOne[j]).size()<1) {
//                    boolean rs = memberService.add(mb);
//                }
//            }
        }
    }

    public List<Member> getAllMember(){
        List<Member> result = memberService.getAllMember();
        return result;
    }

    @POST
    @Path("/rest/all-teacher")
    public List<Teacher> getAllTeacher(){
        List<Teacher> result = memberService.getAllTeacher();
        return result;
    }

    @POST
    @Path("/rest/all-student")
    public List<Student> getAllStudent(){
        List<Student> result = memberService.getAllStudent();
        return result;
    }

    public List<Teacher> getTeacher(String id){
        List<Teacher> result = memberService.getTeacher(id);
        return result;
    }

    @POST
    @Path("/rest/student")
    public List<Student> getStudent(@QueryParam("id") String id){
        id = "3";
        List<Student> result = memberService.getStudent(id);
        return result;
    }
}